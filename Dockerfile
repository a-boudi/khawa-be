FROM ruby:2.6.6-alpine
LABEL maintainer="Abderrahmane Boudi <abr.boudi@gmail.com>"
RUN apk add --no-cache --update less bash build-base \
                                linux-headers \
                                git \
                                postgresql-dev \
                                nodejs \
                                tzdata

ENV INSTALL_PATH /khawa-be
RUN mkdir -p $INSTALL_PATH
WORKDIR $INSTALL_PATH

ENV BUNDLE_PATH=/bundle \
    BUNDLE_BIN=/bundle/bin \
    GEM_HOME=/bundle
RUN gem install bundler
ENV PATH="${BUNDLE_BIN}:${PATH}"

COPY . $INSTALL_PATH
COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
